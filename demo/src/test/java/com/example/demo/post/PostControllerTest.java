package com.example.demo.post;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PostControllerTest {

    //need more tests

    //   Commented because this isn't working to retrieve server port
    //    @LocalServerPort
    //    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    String baseUrl = "http://localhost:8080/api";

    @Test
    void returnAllPosts() throws Exception {
        assertFalse(this.restTemplate.getForObject((baseUrl+"/posts/all"), List.of().getClass(), List.class).isEmpty());
    }
}
