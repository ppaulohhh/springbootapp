package com.example.demo.frontpage;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class home {
    @GetMapping("/home")
    public String home(){
        return "home";
    }

}
