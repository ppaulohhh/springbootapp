package com.example.demo.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PostService {

    private final PostRepository postRepository;

    @Autowired
    public PostService(PostRepository postRepository){
        this.postRepository = postRepository;
    }

    public List<Post> getAll(){
        return postRepository.findAll();
    }
    public Optional<Post> postById(long id){ return postRepository.findById(id); }
    public Optional<Post> postByCity(String city){ return postRepository.findByCity(city);}

    public void newPost(Post post){ postRepository.save(post); }

    public void deletePost(long postId){ postRepository.deleteById(postId);}
}

