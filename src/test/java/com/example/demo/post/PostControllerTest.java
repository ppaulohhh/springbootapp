package com.example.demo.post;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;


@ExtendWith(SpringExtension.class)
public class PostControllerTest {
    @InjectMocks
    PostService postService;

    @Mock
    PostRepository postRepository;

    @BeforeEach
    void setup() {
        BDDMockito.when(postRepository.findAll())
                .thenReturn(List.of(new Post(2L,
                        "Belo Horizonte",
                        "PH",
                        "Cidades",
                        "Primeira cidade planejada do Brasil, Belo Horizonte se caracteriza pela efervescência cultural, qualidade de vida e a hospitalidade de seu povo. A arquitetura diversificada mescla o clássico e o contemporâneo, e confere uma atmosfera de metrópole, sem comprometer seu jeito mineiro. A capital de Minas Gerais é cercada pelas montanhas da Serra do Curral, que encanta pelo belíssimo horizonte e garante um clima agradável aos habitantes.",
                        "-19.9191°",
                        "-43.9387°",
                        "https://direcional.com.br/wp-content/uploads/2020/08/morar-em-Belo-Horizonte.jpg",
                        "Belo Horizonte")));

        BDDMockito.when(postRepository.findById(2L))
                .thenReturn(Optional.of(new Post(2L,
                        "Belo Horizonte",
                        "PH",
                        "Cidades",
                        "Primeira cidade planejada do Brasil, Belo Horizonte se caracteriza pela efervescência cultural, qualidade de vida e a hospitalidade de seu povo. A arquitetura diversificada mescla o clássico e o contemporâneo, e confere uma atmosfera de metrópole, sem comprometer seu jeito mineiro. A capital de Minas Gerais é cercada pelas montanhas da Serra do Curral, que encanta pelo belíssimo horizonte e garante um clima agradável aos habitantes.",
                        "-19.9191°",
                        "-43.9387°",
                        "https://direcional.com.br/wp-content/uploads/2020/08/morar-em-Belo-Horizonte.jpg",
                        "Belo Horizonte")));

        BDDMockito.when(postRepository.findByCity("Belo Horizonte"))
                .thenReturn(Optional.of(new Post(2L,
                        "Belo Horizonte",
                        "PH",
                        "Cidades",
                        "Primeira cidade planejada do Brasil, Belo Horizonte se caracteriza pela efervescência cultural, qualidade de vida e a hospitalidade de seu povo. A arquitetura diversificada mescla o clássico e o contemporâneo, e confere uma atmosfera de metrópole, sem comprometer seu jeito mineiro. A capital de Minas Gerais é cercada pelas montanhas da Serra do Curral, que encanta pelo belíssimo horizonte e garante um clima agradável aos habitantes.",
                        "-19.9191°",
                        "-43.9387°",
                        "https://direcional.com.br/wp-content/uploads/2020/08/morar-em-Belo-Horizonte.jpg",
                        "Belo Horizonte")));
    }


    @Test
    @DisplayName(value = "Checa se é possivel retornar posts.")
    void testAll() {
        List<Post> validate = postService.getAll();
        assertFalse(validate.isEmpty());
    }

    @Test
    @DisplayName(value = "Checa se é possivel retornar posts pelo ID.")
    void testById() {
        Optional<Post> validate = postService.postById(2L);
        assertFalse(validate.isEmpty());
    }

    @Test
    @DisplayName(value = "Checa se é possivel retornar posts pela cidade.")
    void testCity() {
        Optional<Post> validate = postService.postByCity("Belo Horizonte");
        assertFalse(validate.isEmpty());
    }



}