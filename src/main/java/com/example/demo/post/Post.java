package com.example.demo.post;

import javax.persistence.*;

@Entity
@Table
public class Post {
    @Id
    @SequenceGenerator(name = "Post_sequence", sequenceName = "Post_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Post_sequence")
    private Long id;
    private String tittle;
    private String author;
    private String tags;
    @Column(columnDefinition = "TEXT")
    private String text;
    private String latitude;
    private String longitude;
    private String photos;
    private String city;

    // Constructors


    public Post() {};

    public Post(Long id, String tittle, String author, String tags, String text, String latitude, String longitude, String photos, String city) {
        this.id = id;
        this.tittle = tittle;
        this.author = author;
        this.tags = tags;
        this.text = text;
        this.latitude = latitude;
        this.longitude = longitude;
        this.photos = photos;
        this.city = city;
    }

    // Functions

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", tittle='" + tittle + '\'' +
                ", author='" + author + '\'' +
                ", tags=" + tags +
                ", text='" + text + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", photos=" + photos +
                ", city='" + city + '\'' +
                '}';
    }

    // Getters and Setters
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getTittle() {
        return tittle;
    }
    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTags() {
        return tags;
    }
    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

    public String getLatitude() {
        return latitude;
    }
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhotos() {
        return photos;
    }
    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
}
