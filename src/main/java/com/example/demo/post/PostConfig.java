package com.example.demo.post;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class PostConfig {
    @Bean
    CommandLineRunner commandLineRunner(PostRepository repository) {
        return args -> {
            Post Natal = new Post(1L,
                    "O que fazer em  Natal",
                    "PH",
                    "Cidades",
                    "Em  Natal , a ensolarada capital do  Rio Grande do Norte , o sol presenteia as pessoas em cerca de 300 dias por ano.  Natal  tem praias maravilhosas, uma gastronomia diversificada e rica em frutos do mar.",
                    "-5.7842°",
                    "-35.2000°",
                    "https://visit.natal.br/assets/img/galeria3-min.jpg",
                    "Natal");
            Post BeloHorizonte = new Post(2L,
                    "Belo Horizonte",
                    "PH",
                    "Cidades",
                    "Primeira cidade planejada do Brasil, Belo Horizonte se caracteriza pela efervescência cultural, qualidade de vida e a hospitalidade de seu povo. A arquitetura diversificada mescla o clássico e o contemporâneo, e confere uma atmosfera de metrópole, sem comprometer seu jeito mineiro. A capital de Minas Gerais é cercada pelas montanhas da Serra do Curral, que encanta pelo belíssimo horizonte e garante um clima agradável aos habitantes.",
                    "-19.9191°",
                    "-43.9387°",
                    "https://direcional.com.br/wp-content/uploads/2020/08/morar-em-Belo-Horizonte.jpg",
                    "Belo Horizonte");
            repository.saveAll(List.of(Natal, BeloHorizonte));
        };
    }
}
