package com.example.demo.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/")
public class PostController {

    private final PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping("/posts/all")
    public List<Post> getAllPosts(){
        return postService.getAll();
    }

    @GetMapping("/posts/id/{id}")
    public Optional<Post> getPostById(@PathVariable Long id){
        return postService.postById(id);
    }

    @GetMapping("/posts/city/{city}")
    public Optional<Post> getPostById(@PathVariable String city){
        return postService.postByCity(city);
    }

    @PostMapping("/posts/new")
    public void newPost(@RequestBody Post post){ postService.newPost(post); }

    @DeleteMapping("/posts/delete/{postId}")
    public void deletePost(@PathVariable Long postId){
        postService.deletePost(postId);
    }
}
