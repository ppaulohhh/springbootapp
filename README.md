# springBootApp

	- Java 17
	- MySqlServer

## Getting started

This is a simple spring boot app, at start i dont have an idea of what type of website this site will be, but somethings that i already decided:
	- spring boot based
	- mysql database (hosted in heroku)
	- Hosted in google cloud
	- Sonar tests
	- 

## Tools

	- Intelij idea IDE
	- Mysql server
	- Spring intlzr
	- Docker
	- Sonar
	- Heroku (for database)

## Test and Deploy
	- Pipeline in ci/cd gitlab
	- sonar to tests	
	- kubernets hosted in a google cloud cluster
