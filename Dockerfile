FROM amazoncorretto:17
WORKDIR /usr/app
COPY target/*.jar ./
EXPOSE 8080
CMD ["java", "-jar", "demo-0.0.1-SNAPSHOT.jar"]